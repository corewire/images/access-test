package main

import (
	"fmt"
	"log"
	"net"
	"net/http"
)

// HandleHTTP handles incoming HTTP requests
func HandleHTTP(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello, HTTP client!")
}

// HandleTCP handles incoming TCP connections
func HandleTCP(conn net.Conn) {
	defer conn.Close()
	fmt.Fprintf(conn, "Hello, TCP client!")
}

// HandleUDP handles incoming UDP packets
func HandleUDP(conn *net.UDPConn) {
	buf := make([]byte, 1024)
	for {
		n, addr, err := conn.ReadFromUDP(buf)
		if err != nil {
			log.Println("Error while reading UDP packet:", err)
			continue
		}
		fmt.Printf("Received UDP packet from %s: %s\n", addr, string(buf[:n]))
		conn.WriteToUDP([]byte("Hello, UDP client!"), addr)
	}
}

func main() {
	// Start HTTP server
	http.HandleFunc("/", HandleHTTP)
	go func() {
		log.Println("HTTP server started on 0.0.0.0:8080")
		if err := http.ListenAndServe("0.0.0.0:8080", nil); err != nil {
			log.Fatal("HTTP server error:", err)
		}
	}()

	// Start TCP server
	go func() {
		listener, err := net.Listen("tcp", "0.0.0.0:8081")
		if err != nil {
			log.Fatal("TCP server error:", err)
		}
		log.Println("TCP server started on 0.0.0.0:8081")
		for {
			conn, err := listener.Accept()
			if err != nil {
				log.Println("Error accepting connection:", err)
				continue
			}
			go HandleTCP(conn)
		}
	}()

	// Start UDP server
	go func() {
		addr, err := net.ResolveUDPAddr("udp", "0.0.0.0:8082")
		if err != nil {
			log.Fatal("UDP server error:", err)
		}
		conn, err := net.ListenUDP("udp", addr)
		if err != nil {
			log.Fatal("UDP server error:", err)
		}
		log.Println("UDP server started on 0.0.0.0:8082")
		HandleUDP(conn)
	}()

	// Prevent program from exiting
	select {}
}
