import requests
import socket
import os

HTTP_URL = os.getenv("HTTP_URL", "http://127.0.0.1:8080")
TCP_HOST = os.getenv("TCP_HOST", "127.0.0.1:8081")
UDP_HOST = os.getenv("UDP_HOST", "127.0.0.1:8082")

def test_http():
    try:
        response = requests.get('http://127.0.0.1:8080')
        if response.status_code == 200:
            print("HTTP test passed!")
        else:
            print("HTTP test failed!")
    except Exception as e:
        print(f"HTTP test failed due to exception: {e}")

def test_tcp():
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect(("127.0.0.1", 8081))
            data = s.recv(1024).decode("utf-8")
            if "Hello, TCP client!" in data:
                print("TCP test passed!")
            else:
                print("TCP test failed!")
    except Exception as e:
        print(f"TCP test failed due to exception: {e}")

def test_udp():
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as s:
            s.sendto(b"Test UDP packet", ("127.0.0.1", 8082))
            data, _ = s.recvfrom(1024)
            if b"Hello, UDP client!" in data:
                print("UDP test passed!")
            else:
                print("UDP test failed!")
    except Exception as e:
        print(f"UDP test failed due to exception: {e}")

if __name__ == "__main__":
    test_http()
    test_tcp()
    test_udp()
