# Use the official Go image to build the service
FROM golang:1.21-alpine AS builder

# Set the working directory inside the container
WORKDIR /app

# Copy the Go files into the container
COPY . .

# Build the Go app inside the container
RUN CGO_ENABLED=0 GOOS=linux go build -o access-test .

# Use a minimal image to run the service
FROM scratch as final

# Copy the compiled Go app from the builder container
COPY --from=builder /app/access-test /app/access-test

# Expose ports for HTTP, TCP, and UDP
EXPOSE 8080 8081 8082

# Run the service
CMD ["/app/access-test"]