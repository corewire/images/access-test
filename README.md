# access-test


# Test Service in Go

A minimal service written in Go that supports HTTP, TCP, and UDP connections. The service listens on `0.0.0.0` on ports `8080` for HTTP, `8081` for TCP, and `8082` for UDP.

## Prerequisites

- Go (v1.17 or newer)
- Docker

## Running Locally

To run this service locally, you can clone the repository and navigate to the directory containing `main.go`.

```bash
# Clone the repository
go run main.go
``` 

# Building and Running with Docker
To build and run the service as a Docker container, you'll need Docker installed on your machine. Navigate to the directory containing the Dockerfile and run the following commands:

``` 
# Build the Docker image
docker build -t access-test .

# Run the Docker container
docker run --rm -p 8080:8080 -p 8081:8081 -p 8082:8082/udp access-test
```

# Testing the Service

## HTTP
To test the HTTP endpoint, you can use `curl` or a similar tool to send a request to the service. The service will respond with a JSON object containing the request method, URL, and headers.

```bash
curl -X GET http://localhost:8080
```

## TCP
To test the TCP endpoint, you can use `telnet` or a similar tool to send a request to the service. The service will respond with a JSON object containing the request method, URL, and headers.

```bash
telnet localhost 8081
```

## UDP
To test the UDP endpoint, you can use `nc` or a similar tool to send a request to the service. The service will respond with a JSON object containing the request method, URL, and headers.

```bash
echo "Hello, world!" | nc -u -w1 localhost 8082
```